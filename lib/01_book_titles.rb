class Book
   attr_writer :title
   LITTLE_WORDS = ["to", "a", "and", "the", "of", "in", "an"]
   def title 
      @title.capitalize.split.map do |word|
       LITTLE_WORDS.include?(word) ? word : word.capitalize
     end.join(" ")
   end 
end
