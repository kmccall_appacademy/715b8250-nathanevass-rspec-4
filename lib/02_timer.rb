class Timer
  attr_accessor :seconds 
  
  def initialize(seconds = 0)
    @seconds = 0 
  end 
  
  def time_string
    hours = @seconds / 3600 
    minutes = (@seconds % 3600) / 60 
    secs = @seconds % 60
    "#{pad(hours)}:#{pad(minutes)}:#{pad(secs)}"
  end 
  
  def pad(n)
    n.to_s.length == 2 ? n.to_s : "0" << n.to_s 
  end 

end 