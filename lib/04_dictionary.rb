require 'byebug'
class Dictionary
  
  attr_accessor :entries 
  
  def initialize(entries = {})
    @entries = entries 
  end 
  
  def add(entry)
    if entry.is_a? Hash 
      @entries = @entries.merge(entry)
    else 
      @entries[entry] = nil 
    end
  end 
  
  def keywords 
    @entries.keys.sort 
  end 
  
  def include?(word)
    keywords.include?(word)
  end 
  
  def find(letters)
    @entries.select do |k, v|
      letters.chars.each_with_index do |ch, i|
        break false if k[i] != ch 
      end 
    end 
  end 
  
  def printable
    keywords.inject([]) {|a, k| a << ["[#{k}] \"#{entries[k]}\""]}.join("\n")
  end 
end
