class Temperature
  
  def initialize(hash = {})
    @temperature = hash 
  end 
  
  def in_fahrenheit 
    @temperature[:f] ? @temperature[:f] : ctof(@temperature[:c])
  end 
  
  def in_celsius 
    @temperature[:c] ? @temperature[:c] : ftoc(@temperature[:f])
  end 
  
  def ftoc(f)
    (f - 32) * (5.0/9) 
  end 

  def ctof(c)
    (c * 9.0/5 + 32)
  end 
  
  def self.from_celsius(c)
    Temperature.new(:c => c)
  end 
  
  def self.from_fahrenheit(f)
    Temperature.new(:f => f)
  end 
  
end

class Celsius < Temperature 
  def initialize(c)
    @temperature = {:c => c}
  end 
end 

class Fahrenheit < Temperature 
  def initialize(f)
    @temperature = {f: f}
  end 
end 