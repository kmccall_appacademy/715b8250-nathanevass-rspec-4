class Friend
    def greeting(to = nil)
      to.nil? ? "Hello!" : "Hello, #{to}!"
    end
    
end
